#include "codeu_test_lib.h"
#include "Islands.h"

#ifndef UNIT_TESTS
#define UNIT_TESTS

void test_emptyGrid();

void tets_noWater(); 

void test_allWater();

void test_allIslands();

void test_wholeRowIslands(); 

void test_wholeColIslands(); 

void test_assignmentExample(); 

#endif
