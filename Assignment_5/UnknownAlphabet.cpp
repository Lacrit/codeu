#include<iostream>
#include<vector>
#include<unordered_map>
#include<unordered_set>
#include<set>
#include<stack>

std::set<char> all_chars;
bool found_alphabet = false;
std::vector<std::string> words = {"ab", "bb", "ba"};
// directed graph
struct Graph{
	std::unordered_map<char, std::vector<char> > adjacency_matrix;
	Graph();
	// add a new edge to the graph between node1 and node2
	void add_edge(char node1, char node2);
};

void find_all_chars()
{
	for (std::string word : words)
		for (char c : word)
			all_chars.insert(c);
}

Graph::Graph()
{ find_all_chars(); }

void Graph::add_edge(char node1, char node2){
	if(adjacency_matrix.find(node1) == adjacency_matrix.end())
		adjacency_matrix[node1] = std::vector<char>();
	if(adjacency_matrix.find(node2) == adjacency_matrix.end())
		adjacency_matrix[node2] = std::vector<char>();
	adjacency_matrix[node1].push_back(node2);
}

// a function to create a graph from the given words
// each node represent a charachter
Graph* create_graph(std::vector<std::string> words, size_t word_num);

// a function to perform topological sort on graph and print the nodes in sorted fashion
void topological_sort(Graph *graph);

// a function to add nodes to a stack using recursive DFS
void DFS(Graph *graph, std::unordered_set<char> *visited_nodes, std::stack<char> *sorted_nodes, char graph_key);


Graph* create_graph(std::vector<std::string> words, size_t word_num){
	Graph* alphabet_graph = new Graph();
	for(size_t word_index = 0; word_index < word_num-1; word_index++){
		std::string word1 = words[word_index];
		std::string word2 = words[word_index+1];
		// loop over all characters, find the first mismatch and create an edge
		for(size_t char_index = 0; char_index < std::min(word1.length(), word2.length()); char_index++){
			if(word1[char_index] != word2[char_index]){
				alphabet_graph->add_edge(word1[char_index], word2[char_index]);
				break;
			}
		}
	}
	return alphabet_graph;
}


void topological_sort(Graph *graph){
	if (found_alphabet)
			return;
	std::stack<char> *sorted_nodes = new std::stack<char>();
	
	// keep track of visited nodes
	std::unordered_set<char> *visited_nodes = new std::unordered_set<char>();
	
	// loop over all nodes and and perform DFS for each of them if unvisited 
	auto end = all_chars.end();
	for(auto node_iter = all_chars.begin(); node_iter != end ;++node_iter){
		if(visited_nodes->find(*node_iter) == visited_nodes->end()){
				DFS(graph, visited_nodes, sorted_nodes, *node_iter);
		}
	}
	found_alphabet = true;
	// print the stack
	while(!sorted_nodes->empty()){
		std::cout << sorted_nodes->top() << " ";	
		sorted_nodes->pop();
	}
}

void DFS(Graph *graph, std::unordered_set<char> *visited_nodes, std::stack<char> *sorted_nodes, char graph_key){
	visited_nodes->insert(graph_key);
	
	// loop over all adjacent nodes of node 'graph_key' and perform DFS for each of them
	auto end = graph->adjacency_matrix[graph_key].end();
	for(auto iter = graph->adjacency_matrix[graph_key].begin(); iter != end; ++iter){
		if(visited_nodes->find(*iter) == visited_nodes->end())
			DFS(graph, visited_nodes, sorted_nodes, *iter);
	}
 
	// if there is not unvisited adjacent node, add the current node to the stack
	sorted_nodes->push(graph_key);
}

int main(){
	//std::vector<std::string> words = {"ART", "RAT", "CAT", "CAR"};
	Graph* alphabet_graph = create_graph(words, words.size());
		topological_sort(alphabet_graph);
	return 0;
}
